/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
import 'regenerator-runtime/runtime';
import 'source-map-support/register';

import { Server } from '@mobilizing/platform/server/Server.js';
import globalSchema from './schemas/globalSchema.js';
import playerSchema from './schemas/playerSchema.js';

const server = new Server();

// avoid server crash
process.on('unhandledRejection', (reason, p) => {
    console.log('> Unhandled Promise Rejection');
    console.log(reason);
});

// function to allow for async
(async function launch() {
    try {
        // -------------------
        // launch application
        // -------------------
        await server.init();
        await server.start();

        //register schema    
        server.stateManager.registerSchema('global', globalSchema);
        server.stateManager.registerSchema('player', playerSchema);

        // single instance of global schema, created here
        const globalState = await server.stateManager.create('global');

        //playerStates
        const playerStates = new Set();

        const preLoad = (playerState) => {
            // nothing before preLoad

            playerState.set({
                preLoad: true,
            });
        };

        // new client
        server.stateManager.observe(async (schemaName, stateId, nodeId) => {
            switch (schemaName) {
                case 'player': {
                    // new player client is created client-side
                    // attach server-side
                    const playerState = await server.stateManager.attach(schemaName, stateId);

                    // keep a set of all players
                    playerStates.add(playerState);

                    playerState.subscribe((updates) => {

                        if (playerStates.size > 0) {

                        }
                        //console.log(globalState.get("meanAcc"));

                    }); // udpdates

                    // logic to do when the state is deleted
                    // (e.g. when the player disconnects)
                    playerState.onDetach(() => {
                        // clean things
                        playerStates.delete(playerState);
                    });

                    break;
                } // 'player'

                default:
                    break;

            }
        });

        // previously created server-side
        globalState.subscribe((updates) => {
            // nothing here
        });

    }
    catch (err) {
        console.error(err.stack);
    }
})();

