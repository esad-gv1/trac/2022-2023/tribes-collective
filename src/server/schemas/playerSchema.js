export const playerSchema = {
    id: {
        type: 'integer',
        default: null,
        nullable: true,
    },

    device: {
        type: "any",
        default: null,
        nullable: true,
        tribeName: null,
        name: null,
        effects: [],
    },
};

export default playerSchema;
