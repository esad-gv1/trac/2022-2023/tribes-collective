export const globalSchema = {

    reload: {
        type: 'boolean',
        event: true,
    },

    debug: {
        type: 'boolean',
        default: false,
    },
};

export default globalSchema;
