import * as Mobilizing from '@mobilizing/library';

export class Symbol {
    /**
     * A Symbol represents an abstract data structure that can be animated (tweening). The target field contains the value to manipulate, the from field gives the starting value and the to field give the ending ones. A duration in ms must be given. The easing field can be specified to change the interpolation between values during time.
     * This class uses a Mobilizing Animation Component.
     * 
     * @param {Object} params
     * @param {Object} params.context Mobilizing context to use
     * @param {Object} params.target the target object containing all the values to change
     * @param {Object} params.from the to object containing all the values to begin with
     * @param {Object} params.to the to object containing all the values to end with
     * @param {Object} params.duration the duration of the tweening in ms
     * @param {Number} params.from the to object containing all the values to begin with.
     * @param {Number} [params.repeat=0] The number of times the animation should be repeated, set to Infinity to repeat indefinately
     * @param {Number} params.easing which easing to use for interpolation :
    "linear"
    "easeInQuad"
    "easeOutQuad"
    "easeInOutQuad"
    "easeInCubic"
    "easeOutCubic"
    "easeInOutCubic"
    "easeInQuart"
    "easeOutQuart"
    "easeInOutQuart"
    "easeInQuint"
    "easeOutQuint"
    "easeInOutQuint"
    "easeInSin"
    "easeOutSin"
    "easeInOutSin"
    "easeInElastic"
    "easeOutElastic"
    "easeInOutElastic"
    "easeInBounce"
    "easeOutBounce"
    "easeInOutBounce"
     * 
     */
    constructor({
        context = undefined,
        target = undefined,
        from = null,
        to = null,
        duration = null,
        easing = Animation.Easing.linear,
        repeat = 0,
    }) {
        this.context = context;
        this.target = target;
        this.from = from;
        this.to = to;
        this.duration = duration;
        this.easing = easing;
        this.repeat = repeat;
    }

    setContext(context) {
        this.context = context;
    }

    setTarget(target) {
        this.target = target;
    }

    setOnFinishCallback(callback) {
        this.onFinishCallback = callback;
    }

    setDuration(duration) {
        this.duration = duration;
        //console.log("duration",duration);
        if (this.animation) {
            this.animation.duration = this.duration;
        }
    }

    setFrom(from) {
        this.from = from;
        if (this.animation) {
            this.animation.from = this.from;
        }
    }

    stop() {
        if (this.animation) {
            this.animation.stop();
        }
    }

    setup() {

        this.animation = new Mobilizing.Animation({
            target: this.target,
            from: this.from,
            to: this.to,
            duration: this.duration,
            easing: this.easing,
            repeat: this.repeat,
            "onUpdate": (target) => {
                //console.log("target", target);
            },
            "onFinish": () => {
                //console.log("onFinish", this);
                if (this.onFinishCallback) {
                    this.onFinishCallback(this);
                }
            }
        });

        this.context.addComponent(this.animation);
        this.animation.setup();
        this.animation.on();
    }
}
