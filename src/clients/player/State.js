export class State {

    constructor({
        name = undefined,
        symbols = [],
        loop = true,
    } = {}) {

        this.name = name;
        this.isPlaying = false;

        this.symbols = [];

        if (symbols) {
            if (symbols.length > 0) {
                this.addSymbols(symbols);
            }
        }
        this.loop = loop;
        this.currentSymbol = undefined;
    }

    addSymbols(array) {

        array.forEach((symbol) => {
            this.addSymbol(symbol);
        });
    }

    addSymbol(symbol) {
        //adds the callback to finished events
        if (this.onFinishCallback) {
            //console.log(this.onFinishCallback);
            symbol.setOnFinishCallback(this.onFinishCallback);
        }

        //get the previous symbol in the list, if any
        const prevSymbol = this.symbols[this.symbols.length - 1];

        //auto fill "from" prop if not already
        if (!symbol.from) {
            symbol.setFrom(prevSymbol.to);
        }

        //push the new symbol in the array
        this.symbols.push(symbol);
        //keep track of the last symbol added
        this.lastSymbol = symbol;

        //manage play chainning of all symbols
        if (this.symbols.length > 1) {
            prevSymbol.animation.events.on("finish", () => {
                //console.log("play next", symbol.animation);
                symbol.animation.play();
            });
        }

        //update some states
        symbol.animation.events.on("start", (target) => {
            this.currentSymbol = symbol;
            //console.log("start", this.currentSymbol,target);
        });

        //manage loop
        symbol.animation.events.on("finish", (target) => {
            if (this.loop && this.lastSymbol === this.currentSymbol) {
                this.loopPlay();
            }
        });
    }
    /**
     * used for loop play, to avoid auto cancel of loop because of isPlaying
     */
    loopPlay(){
        this.symbols[0].animation.play();
    }

    play() {
        if (!this.isPlaying) {
            this.symbols[0].animation.play();
            this.isPlaying = true;
        }
    }

    stop() {
        if (this.currentSymbol) {
            this.currentSymbol.stop();
            this.isPlaying = false;
        }
    }

}
