/* eslint-disable no-case-declarations */
import * as Mobilizing from '@mobilizing/library';
import { VisualLuminanceLuminanceState } from "./states/visual/VisualLuminanceLuminanceState.js";
import { VisualLuminanceSoundState } from "./states/visual/VisualLuminanceSound.js";
import { VisualLuminanceVibrationState } from "./states/visual/VisualLuminanceVibration.js";

import { AudioLuminanceState } from './states/sound/AudioLuminanceState.js';
import { AudioSoundState } from './states/sound/AudioSoundState.js';
import { AudioVibrationState } from './states/sound/AudioVibrationState.js';

export class PlayerScript {

    constructor({
        reportCallback,
    } = {}) {
        this.reportCallback = reportCallback;
        this.needsUserInteraction = true;

        //base props of any player
        this.tribeName = null;
        this.name = null;
        this.effects = [];

        this.showDebugDiv = true;
        this.debugDiv = document.createElement("div");
        this.debugDiv.className = "debugDiv";
        if (this.showDebugDiv) {
            document.body.appendChild(this.debugDiv);
        }
    }

    preLoad(loader) {
        //load all the media you need before setup
        //audio
        const luminanceSoundFiles = [
            "L1_0.mp3",
            // "L1_1.mp3",
            "L2_0.mp3",
            // "L2_1.mp3",
            "L3_0.mp3",
            // "L3_1.mp3",
        ];
        const soundSoundFiles = [
            "S1.mp3",
            "S2.mp3",
            "S3.mp3",
        ];
        const vibrationSoundFiles = [
            "V1_0.mp3",
            // "V1_1.mp3",
            "V2_0.mp3",
            // "V2_1.mp3",
            "V3_0.mp3",
            // "V3_1.mp3",
        ];

        const testSoundFile = "piste-filtre-test.mp3";

        this.luminanceBufferRequests = [];
        this.soundBufferRequests = [];
        this.vibrationBufferRequests = [];

        for (let i = 0; i < luminanceSoundFiles.length; i++) {
            const soundName = luminanceSoundFiles[i];
            const request = loader.loadArrayBuffer({ url: `./assets/audio/${soundName}` });
            this.luminanceBufferRequests.push(request);
        }

        for (let i = 0; i < soundSoundFiles.length; i++) {
            const soundName = soundSoundFiles[i];
            const request = loader.loadArrayBuffer({ url: `./assets/audio/${soundName}` });
            this.soundBufferRequests.push(request);
        }

        for (let i = 0; i < vibrationSoundFiles.length; i++) {
            const soundName = vibrationSoundFiles[i];
            const request = loader.loadArrayBuffer({ url: `./assets/audio/${soundName}` });
            this.vibrationBufferRequests.push(request);
        }

        this.testBufferRequest = loader.loadArrayBuffer({ url: `./assets/audio/${testSoundFile}` });

        //visual texture
        this.haloRequest = loader.loadImage({ "url": "./assets/img/halo4.png" });
    }

    setup() {
        //console.log(this.luminanceBufferRequests, this.soundBufferRequests, this.vibrationBufferRequests);

        this.renderer = new Mobilizing.three.RendererThree();
        this.context.addComponent(this.renderer);

        this.camera = new Mobilizing.three.Camera();
        this.camera.setToPixel();
        //this.camera.transform.setLocalPosition(0, 0, 3000);
        this.renderer.addCamera(this.camera);

        this.firstEffectNode = this.makeFirstEffect();
        this.renderer.addToCurrentScene(this.firstEffectNode);

        this.secondEffectNode = this.makeSecondEffect();
        this.renderer.addToCurrentScene(this.secondEffectNode);

        //mobilizing audio service
        this.audioRenderer = new Mobilizing.audio.Renderer();
        this.context.addComponent(this.audioRenderer);

        this.audioContext = this.audioRenderer.audioContext;
        this.biquadFilter = this.audioContext.createBiquadFilter();
        this.biquadFilter.type = "bandpass";
        //"no effect values"
        this.biquadFilter.frequency.value = 350;
        this.biquadFilter.Q.value = 1;//0.8;
        this.audioRenderer.setup();

        //sound sources creation from buffers
        this.soundSourcesMap = new Map();

        //get the specific buffer request we should use for this player sound
        let bufferRequestArray = null;
        if (this.tribeName === "luminance") {
            bufferRequestArray = this.luminanceBufferRequests;
        }
        else if (this.tribeName === "sound") {
            bufferRequestArray = this.soundBufferRequests;
        }
        else if (this.tribeName === "vibration") {
            bufferRequestArray = this.vibrationBufferRequests;
        }

        //get the player index to get the corresponding sound
        const tribeIndex = Number(this.name.substring(this.name.length - 1, this.name.length));
        console.log("bufferRequestArray", bufferRequestArray, "tribeIndex", tribeIndex)

        //audio
        const source = new Mobilizing.audio.Source({
            "renderer": this.audioRenderer,
            "is3D": false,
            "loop": true
        });
        this.context.addComponent(source);

        console.log();

        const buffer = bufferRequestArray[tribeIndex].getValue();
        //const buffer = this.testBufferRequest.getValue();

        const sound = new Mobilizing.audio.Buffer({
            "renderer": this.audioRenderer,
            "arrayBuffer": buffer,
            "decodedCallback": () => {
                //attach the buffer to the sound
                source.setBuffer(sound);
                source.play();
                //connect things
                //setTimeout(() => {
                //rebuild nodes to add filter
                console.log(source, source.getNativeSource());
                const src = source.getNativeSource();
                source.disconnect();
                src.connect(this.biquadFilter);
                this.biquadFilter.connect(this.audioContext.destination);
                //source.gain.connect(this.audioContext.destination);

                console.log("biquadFilter", this.biquadFilter, source);
                //}, 1000);

            }
        });

        //sound states
        this.luminanceSound = { frequency: 1500, Q: 1 };
        this.soundSound = { frequency: 1500, Q: 1 };
        this.vibrationSound = { frequency: 1500, Q: 1 };

        this.luminanceSoundState = new AudioLuminanceState({
            context: this.context,
            target: this.luminanceSound,
        });

        this.soundSoundState = new AudioSoundState({
            context: this.context,
            target: this.soundSound,
        });

        this.vibrationSoundState = new AudioVibrationState({
            context: this.context,
            target: this.vibrationSound,
        });

        //visual states
        this.backgroundLuminance = { hue: 0, saturation: 0, majorLuminance: 0 };
        this.firstEffectLuminance = { hue: 0, saturation: 0, minorLuminance: 0 };
        this.secondEffectLuminance = { hue: 0, saturation: 0, minorLuminance: 0 };

        //main states map for luminance
        this.majorStates = this.makeLuminanceVisualeStateMap(this.backgroundLuminance);
        this.firstMinorStates = this.makeLuminanceVisualeStateMap(this.firstEffectLuminance);
        this.secondMinorStates = this.makeLuminanceVisualeStateMap(this.secondEffectLuminance);

        //console.log(this.states);

        //first configuration of this player
        this.updateStates();
    }

    //states generation
    makeLuminanceVisualeStateMap(target) {

        //map to be filled and returned
        const map = new Map();

        const luminanceStatesInstances = [];//local array for luminance states to build
        //luminanceLuminanceState
        const luminanceLuminanceState = new VisualLuminanceLuminanceState({
            context: this.context,
            target,
            /* onFinishCallback: (symbol) => {
                symbol.setDuration(Mobilizing.math.randomFromTo(200, 2000));
            }, */
        });
        //add the state instance to the local array
        luminanceStatesInstances.push(luminanceLuminanceState);

        //luminanceSoundState
        const luminanceSoundState = new VisualLuminanceSoundState({
            context: this.context,
            target,
            /* onFinishCallback: (symbol) => {
                symbol.setDuration(Mobilizing.math.randomFromTo(200, 2000));
            }, */
        });
        //add the state instance to the local array
        luminanceStatesInstances.push(luminanceSoundState);

        //LuminanceVibrationState
        const luminanceVibrationState = new VisualLuminanceVibrationState({
            context: this.context,
            target,
            /* onFinishCallback: (symbol) => {
                symbol.setDuration(Mobilizing.math.randomFromTo(200, 2000));
            }, */
        });
        //add the state instance to the local array
        luminanceStatesInstances.push(luminanceVibrationState);

        //add the state instance to the map
        map.set("luminance", luminanceStatesInstances);
        return map;
    }

    makeFirstEffect() {
        const effectNode = new Mobilizing.three.Node();
        const pageSize = { width: window.innerWidth, height: window.innerHeight };

        const halo = this.haloRequest.getValue();

        let wMult = 0.8;
        let hMult = 1.1;
        const left = new Mobilizing.three.Plane({
            "width": pageSize.width * wMult,
            "height": pageSize.height * hMult,
            "material": "basic",
        });
        effectNode.transform.addChild(left.transform);
        let texture = new Mobilizing.three.Texture({ "image": halo });
        left.material.setTransparent(true);
        left.material.setTexture(texture);

        const right = new Mobilizing.three.Plane({
            "width": pageSize.width * wMult,
            "height": pageSize.height * hMult,
            "material": "basic",
        });
        effectNode.transform.addChild(right.transform);
        texture = new Mobilizing.three.Texture({ "image": halo });
        right.material.setTransparent(true);
        right.material.setTexture(texture);

        wMult = 0.8;
        hMult = 0.5;
        const top = new Mobilizing.three.Plane({
            "width": pageSize.width * wMult,
            "height": pageSize.height * hMult,
            "material": "basic",
        });
        effectNode.transform.addChild(top.transform);
        texture = new Mobilizing.three.Texture({ "image": halo });
        top.material.setTransparent(true);
        top.material.setTexture(texture);

        const bottom = new Mobilizing.three.Plane({
            "width": pageSize.width * wMult,
            "height": pageSize.height * hMult,
            "material": "basic",
        });
        effectNode.transform.addChild(bottom.transform);
        texture = new Mobilizing.three.Texture({ "image": halo });
        bottom.material.setTransparent(true);
        bottom.material.setTexture(texture);

        left.transform.setLocalPosition(0, -pageSize.height / 2);
        right.transform.setLocalPosition(pageSize.width, -pageSize.height / 2);
        top.transform.setLocalPosition(pageSize.width / 2, 0);
        bottom.transform.setLocalPosition(pageSize.width / 2, -pageSize.height);

        return effectNode;
    }

    makeSecondEffect() {
        const effectNode = new Mobilizing.three.Node();
        const pageSize = { width: window.innerWidth, height: window.innerHeight };

        const halo = this.haloRequest.getValue();

        let wMult = 1;
        let hMult = 1;
        const leftTop = new Mobilizing.three.Plane({
            "width": pageSize.width * wMult,
            "height": pageSize.height * hMult,
            "material": "basic",
        });
        effectNode.transform.addChild(leftTop.transform);
        let texture = new Mobilizing.three.Texture({ "image": halo });
        leftTop.material.setTransparent(true);
        leftTop.material.setTexture(texture);

        const rightTop = new Mobilizing.three.Plane({
            "width": pageSize.width * wMult,
            "height": pageSize.height * hMult,
            "material": "basic",
        });
        effectNode.transform.addChild(rightTop.transform);
        texture = new Mobilizing.three.Texture({ "image": halo });
        rightTop.material.setTransparent(true);
        rightTop.material.setTexture(texture);

        const leftBottom = new Mobilizing.three.Plane({
            "width": pageSize.width * wMult,
            "height": pageSize.height * hMult,
            "material": "basic",
        });
        effectNode.transform.addChild(leftBottom.transform);
        texture = new Mobilizing.three.Texture({ "image": halo });
        leftBottom.material.setTransparent(true);
        leftBottom.material.setTexture(texture);

        const rightBottom = new Mobilizing.three.Plane({
            "width": pageSize.width * wMult,
            "height": pageSize.height * hMult,
            "material": "basic",
        });
        effectNode.transform.addChild(rightBottom.transform);
        texture = new Mobilizing.three.Texture({ "image": halo });
        rightBottom.material.setTransparent(true);
        rightBottom.material.setTexture(texture);

        leftTop.transform.setLocalPosition(0, 0);
        rightTop.transform.setLocalPosition(pageSize.width, 0);
        leftBottom.transform.setLocalPosition(0, -pageSize.height);
        rightBottom.transform.setLocalPosition(pageSize.width, -pageSize.height);

        return effectNode;
    }

    update() {

        /* AUDIO UPDATES */
        if (this.biquadFilter) {
            if (this.luminanceSoundState.isPlaying) {
                const freq = Mobilizing.math.clamp(this.luminanceSound.frequency, 0, 2205);
                const q = Mobilizing.math.clamp(this.luminanceSound.Q, 0.0001, 1000);
                this.biquadFilter.frequency.value = freq;
                this.biquadFilter.Q.value = q;
            }
            else if (this.soundSoundState.isPlaying) {
                const freq = Mobilizing.math.clamp(this.soundSound.frequency, 0, 2205);
                const q = Mobilizing.math.clamp(this.soundSound.Q, 0.0001, 1000);
                this.biquadFilter.frequency.value = freq;
                //this.biquadFilter.Q.value = q;
            }
            else if (this.vibrationSoundState.isPlaying) {
                const freq = Mobilizing.math.clamp(this.vibrationSound.frequency, 0, 2205);
                const q = Mobilizing.math.clamp(this.vibrationSound.Q, 0.0001, 1000);
                //this.biquadFilter.frequency.value = freq;
                this.biquadFilter.Q.value = q;
            }

        }

        /* VISUAL UPDATES */
        //const color = this.makeHSLCSSColor(this.lumiHsl);
        //const backColor = new Mobilizing.three.Color(this.backgroundLuminance.hue / 360, this.backgroundLuminance.saturation / 100, this.backgroundLuminance.majorLuminance / 100);
        const backColor = new Mobilizing.three.Color();
        let h = Mobilizing.math.clamp(Math.floor(this.backgroundLuminance.hue), 0, 360);
        let s = Mobilizing.math.clamp(Math.floor(this.backgroundLuminance.saturation), 0, 100);
        let l = Mobilizing.math.clamp(Math.floor(this.backgroundLuminance.majorLuminance), 0, 100);
        backColor.setStyle(`hsl(${h}, ${s}%, ${l}%)`);
        this.renderer.setClearColor(backColor);

        const firstEffectColor = this.firstEffectNode.transform.children[0].material.getColor();
        h = Mobilizing.math.clamp(Math.floor(this.firstEffectLuminance.hue), 0, 360);
        s = Mobilizing.math.clamp(Math.floor(this.firstEffectLuminance.saturation), 0, 100);
        l = Mobilizing.math.clamp(Math.floor(this.firstEffectLuminance.minorLuminance), 0, 100);
        firstEffectColor.setStyle(`hsl(${h}, ${s}%, ${l}%)`);

        this.firstEffectNode.transform.children.forEach((model) => {
            model.material.setColor(firstEffectColor);
        });

        const secondEffectColor = this.secondEffectNode.transform.children[0].material.getColor();
        h = Mobilizing.math.clamp(Math.floor(this.secondEffectLuminance.hue), 0, 360);
        s = Mobilizing.math.clamp(Math.floor(this.secondEffectLuminance.saturation), 0, 100);
        l = Mobilizing.math.clamp(Math.floor(this.secondEffectLuminance.minorLuminance), 0, 100);
        secondEffectColor.setStyle(`hsl(${h}, ${s}%, ${l}%)`);

        this.secondEffectNode.transform.children.forEach((model) => {
            model.material.setColor(secondEffectColor);
        });

    }

    debug() {
        this.debugDiv.innerHTML =
            `
            <p>tribeName = ${this.tribeName}</p>
            <p>name = ${this.name}</p>
            <p>effects = ${this.effects}</p>
        `
    }

    updateStates() {

        if (this.majorStates) {

            //check major states
            const majorState = this.majorStates.get(this.tribeName);
            console.log("majorState for this.tribeName", this.tribeName, "is", majorState);

            //major state
            if (majorState) {
                if (majorState.length > 0) {
                    majorState.forEach((state) => {
                        if (state.name === "visualluminanceluminance") {
                            console.log("will play state", state);
                            state.play();
                        }
                    });
                }
            }
            else {
                //others majors states
                //manage luminance major state for sound & vibration
                const luminanceStates = this.majorStates.get("luminance");
                console.log("luminanceStates", luminanceStates);

                for (let i = 0; i < luminanceStates.length; i++) {
                    const state = luminanceStates[i];
                    if (state.name.indexOf(this.tribeName) > 0) {
                        console.log("will play state", state);
                        state.play();
                        break;
                    }
                }
            }
        }

        let luminanceStates;

        switch (this.effects.length) {
            case 0:
                //stop the effects
                if (this.firstMinorStates && this.secondMinorStates) {
                    luminanceStates = this.firstMinorStates.get("luminance");
                    luminanceStates.forEach((state) => {
                        console.log("will stop effect state", state.name);
                        state.stop();
                    });
                    luminanceStates = this.secondMinorStates.get("luminance");
                    luminanceStates.forEach((state) => {
                        console.log("will stop effect state", state.name);
                        state.stop();
                    });
                }
                /* this.firstEffectDiv.style.visibility = "hidden";
                this.secondEffectDiv.style.visibility = "hidden"; */
                if (this.firstEffectNode) {
                    this.firstEffectNode.setVisible(false);
                    this.secondEffectNode.setVisible(false);
                }
                break;

            case 1:
                if (this.firstMinorStates) {
                    luminanceStates = this.firstMinorStates.get("luminance");
                    console.log("minor luminanceStates", luminanceStates, this.effects);

                    luminanceStates.forEach((state) => {
                        if (state.name === `visualluminance${this.effects[0]}`) {
                            console.log("will play effect state", state.name);
                            state.play();
                        }
                    });
                    /* this.firstEffectDiv.style.visibility = "visible";
                    this.secondEffectDiv.style.visibility = "hidden"; */
                    if (this.firstEffectNode) {
                        this.firstEffectNode.setVisible(true);
                        this.secondEffectNode.setVisible(false);
                    }
                }
                break;

            case 2:

                if (this.firstMinorStates) {
                    luminanceStates = this.firstMinorStates.get("luminance");
                    console.log("minor luminanceStates", luminanceStates, this.effects);

                    luminanceStates.forEach((state) => {
                        if (state.name === `visualluminance${this.effects[0]}`) {
                            console.log("will play effect state", state.name);
                            state.play();
                        }
                    });
                    //this.firstEffectDiv.style.visibility = "visible";
                    if (this.firstEffectNode) {
                        this.firstEffectNode.setVisible(true);
                    }
                }

                if (this.secondMinorStates) {
                    luminanceStates = this.secondMinorStates.get("luminance");
                    console.log("minor luminanceStates", luminanceStates, this.effects);

                    luminanceStates.forEach((state) => {
                        if (state.name === `visualluminance${this.effects[1]}`) {
                            console.log("will play effect state", state.name);
                            state.play();
                        }
                    });
                    //this.secondEffectDiv.style.visibility = "visible";
                    if (this.firstEffectNode) {
                        this.secondEffectNode.setVisible(true);
                    }
                }
                break;
            default:
                break;
        }

        /* AUDIO */
        if (this.luminanceSoundState) {
            if (this.effects.indexOf("luminance") >= 0) {
                this.luminanceSoundState.play();
            }
            else {
                this.luminanceSoundState.stop();
            }
        }

        if (this.soundSoundState) {
            if (this.effects.indexOf("sound") >= 0) {
                this.soundSoundState.play();
            }
            else {
                this.soundSoundState.stop();
            }
        }

        if (this.vibrationSoundState) {
            console.log("vibrationSoundState", this.vibrationSoundState, this.effects);
            if (this.effects.indexOf("vibration") >= 0) {
                this.vibrationSoundState.play();
            }
            else {
                this.vibrationSoundState.stop();
            }

            if (!this.luminanceSoundState.isPlaying && !this.luminanceSoundState.isPlaying && !this.vibrationSoundState.isPlaying) {
                this.biquadFilter.frequency.value = 350;
                this.biquadFilter.frequency.Q = 1;
            }
        }
    }

    apply(updates) {

        const { device } = updates;
        if (device) {
            console.log("apply", device);

            const { tribeName, name, effects } = device;

            if (tribeName) {
                this.tribeName = tribeName;
                this.name = name;
                this.effects = effects;
                this.debug();
                this.updateStates();
            }

        }
    }

    report(updates) {
        if (!this.reportCallback) {
            return;
        }

        // direct binding
        this.reportCallback(updates);
    }
}
