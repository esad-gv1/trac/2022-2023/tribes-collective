
import * as Mobilizing from '@mobilizing/library';
import { Client } from '@mobilizing/platform/client/Client.js';
import { PlayerScript } from './PlayerScript.js';

export default class PlayerClient extends Client {
    constructor({
        features,
        container,
    } = {}) {
        super({
            features,
            container,
        });
        this.client = this;
        this.container = container;
        this.rafId = null;

        this.context = new Mobilizing.Context();
        this.mobilizingScript = new PlayerScript({
            reportCallback: (updates) => this.report(updates), // bind this
        });
    }

    async init() {
        await super.init();
        // features may be available
    }

    async start() {
        await super.start();
        // available features should be ready

        // move soundworks container under
        this.container.style.zIndex = -100;
        this.container.style.position = 'absolute';

        // create a player state
        this.playerState = await this.client.stateManager.create('player', {
            id: this.client.id,
        });

        //get the update for this player coming from display/controller
        this.playerState.subscribe((newValues, oldValues) => {
            console.log(newValues);
            this.mobilizingScript.apply(newValues);
        });

        // register to the already existing global state
        this.globalState = await this.client.stateManager.attach('global');

        this.globalState.subscribe((updates) => {
            const { reload } = updates;
            if (reload) {
                window.location = window.location; // eslint-disable-line
            }

            // direct binding to mobilizing script
            this.mobilizingScript.apply(updates);
        });

        //check the script for the need of user interaction
        if (this.mobilizingScript.needsUserInteraction) {
            this.context.userInteractionDone.setup();
            await this.context.userInteractionDone.promise;
        }
        this.context.addComponent(this.mobilizingScript);
        this.runner = new Mobilizing.Runner({
            context: this.context,
        });
    }

    report(updates) {
        // direct binding
        //console.log("updates", updates);
        this.playerState.set(updates);
    }

}
