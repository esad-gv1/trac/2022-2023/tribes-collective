import { State } from "../State.js";
import { Symbol } from "../Symbol.js";

export class VolumeState extends State {

    constructor({
        context = undefined,
        target = undefined,
        loop = true,
        onFinishCallback = null
    } = {}) {
        super({ loop });

        this.name = this.constructor.name.toLowerCase().replace("state", "");

        this.context = context;
        this.target = target;
        this.onFinishCallback = onFinishCallback;

        const allEasing = Mobilizing.Animation.Easing.easeInOutCubic;

        //console.log("this.target", this.target);
        const symbols = [

            new Symbol({
                from: { v1: 0, v2: 0, v3: 0, f: 30 },
                to: { v1: .1, v2: 0.2, v3: 0, f: 5000 },
                duration: 500,
                easing: allEasing,
            }),

            new Symbol({
                //from: { v: 100 },
                to: { v1: 0, v2: 0.5, v3: 0.8, f: 30 },
                duration: 500,
                easing: allEasing,
            }),

            /* new Symbol({
                // from: { v: 0 },
                to: { v1: .8, v2: 0, v3: 0.2, f: 5000 },
                duration: 2000,//Mobilizing.math.randomFromTo(100, 1000),
                easing: allEasing,
            }),

            new Symbol({
                // from: { v: 100 },
                to: { v1: 0, v2: 1, v3: 0, f: 20000 },
                duration: 2000,//Mobilizing.math.randomFromTo(100, 1000),
                easing: allEasing,
            }), */
        ];

        //auto set of needed props
        symbols.forEach((symbol) => {
            symbol.setContext(this.context);
            symbol.setTarget(this.target);
            symbol.setup(this.context);
        });
        //add symbols
        this.addSymbols(symbols);
    }
}