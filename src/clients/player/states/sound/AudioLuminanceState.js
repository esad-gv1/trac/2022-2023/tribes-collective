import * as Mobilizing from '@mobilizing/library';
import { State } from "../../State.js";
import { Symbol } from "../../Symbol.js";

export class AudioLuminanceState extends State {

    constructor({
        context = undefined,
        target = undefined,
        loop = true,
        onFinishCallback = null
    } = {}) {
        super({ loop });

        this.name = this.constructor.name.toLowerCase().replace("state", "");

        this.context = context;
        this.target = target;
        this.onFinishCallback = onFinishCallback;

        const allEasing = Mobilizing.Animation.Easing.linear;

        //0 -> 22050
        const frequencyEffectValues = [1500, 100, 500, 0, 2000, 1500];
        const QEffectValues = [1, 100, 20, 70, 20, 1];

        const symbols = [];

        const frequencyValueFrom = frequencyEffectValues[0];
        let frequencyValueTo = frequencyEffectValues[1];

        const QValueFrom = QEffectValues[0];
        let QValueTo = QEffectValues[1];

        const duration = 1000;

        const firstSymbol = new Symbol({
            from: { frequency: frequencyValueFrom, Q: QValueFrom },
            to: { frequency: frequencyValueTo, Q: QValueTo },
            duration,
            easing: allEasing,
        });
        symbols.push(firstSymbol);

        for (let i = 2; i < frequencyEffectValues.length; i++) {
            frequencyValueTo = frequencyEffectValues[i];
            QValueTo = QEffectValues[i];

            const symbol = new Symbol({
                //from: { majorLuminance: majorValue, minorLuminance : minorValue},
                to: { frequency: frequencyValueTo, Q: QValueTo },
                duration,
                easing: allEasing,
            });

            symbols.push(symbol);
        }

        //auto set of needed props
        symbols.forEach((symbol) => {
            symbol.setContext(this.context);
            symbol.setTarget(this.target);
            symbol.setup(this.context);
        });
        //add symbols
        this.addSymbols(symbols);

        //console.log(symbols);
    }
}
