import { State } from "../State.js";
import { Symbol } from "../Symbol.js";

export class HueState extends State {

    constructor({
        context = undefined,
        target = undefined,
        loop = true,
        onFinishCallback = null
    } = {}) {
        super({ loop });

        this.name = this.constructor.name.toLowerCase().replace("state", "");

        this.context = context;
        this.target = target;
        this.onFinishCallback = onFinishCallback;

        //console.log("this.target", this.target);
        const symbols = [

            new Symbol({
                from: { h: 0 },
                to: { h: 60 },
                duration: 500,
                easing: Mobilizing.Animation.Easing.linear,
            }),

            new Symbol({
                from: { h: 60 },
                to: { h: 240 },
                duration: 100,
                easing: Mobilizing.Animation.Easing.linear,
            }),

            new Symbol({
                from: { h: 240 },
                to: { h: 120 },
                duration: 200,
                easing: Mobilizing.Animation.Easing.linear,
            }),

            new Symbol({
                from: { h: 120 },
                to: { h: 300 },
                duration: 500,
                easing: Mobilizing.Animation.Easing.linear,
            }),
        ];

        //auto set of needed props
        symbols.forEach((symbol) => {
            symbol.setContext(this.context);
            symbol.setTarget(this.target);
            symbol.setup(this.context);
        });
        //add symbols
        this.addSymbols(symbols);
    }
}