import { State } from "../State.js";
import { Symbol } from "../Symbol.js";

export class ShiveringState extends State {

    constructor({
        context = undefined,
        target = undefined,
        loop = true,
        onFinishCallback = null
    } = {}) {
        super({ loop });

        this.name = this.constructor.name.toLowerCase().replace("state", "");

        this.context = context;
        this.target = target;
        this.onFinishCallback = onFinishCallback;

        const symbols = [

            new Symbol({
                from: { x: 0, y: 0 },
                to: { x: 10, y: 20 },
                duration: 100,
                easing: Mobilizing.Animation.Easing.linear,
            }),

            new Symbol({
                from: { y: 20 },
                to: { y: -10 },
                duration: 100,
                easing: Mobilizing.Animation.Easing.linear,
            }),

            new Symbol({
                from: { x: 10, y: -10 },
                to: { x: 0, y: 0 },
                duration: 100,
                easing: Mobilizing.Animation.Easing.linear,
            })
        ];

        //auto set of needed props
        symbols.forEach((symbol) => {
            symbol.setContext(this.context);
            symbol.setTarget(this.target);
            symbol.setup(this.context);
        });
        //add symbols
        this.addSymbols(symbols);
    }
}