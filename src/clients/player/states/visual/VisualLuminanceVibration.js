import * as Mobilizing from '@mobilizing/library';
import { State } from "../../State.js";
import { Symbol } from "../../Symbol.js";

export class VisualLuminanceVibrationState extends State {

    constructor({
        context = undefined,
        target = undefined,
        loop = true,
        onFinishCallback = null
    } = {}) {
        super({ loop });

        this.name = this.constructor.name.toLowerCase().replace("state", "");

        this.context = context;
        this.target = target;
        this.onFinishCallback = onFinishCallback;

        const allEasing = Mobilizing.Animation.Easing.linear;

        const majorEffectValues = [0, 64, 0, 30, 0];
        const minorEffectValues = [0, 30, 0, 10, 0];

        const symbols = [];

        const majorValueFrom = majorEffectValues[0];
        const minorValueFrom = minorEffectValues[0];
        let majorValueTo = majorEffectValues[1];
        let minorValueTo = minorEffectValues[1];

        const duration = 500;

        const hue = 133;
        const saturation = 56;

        const firstSymbol = new Symbol({
            from: {hue, saturation, majorLuminance: majorValueFrom, minorLuminance: minorValueFrom },
            to: {hue, saturation, majorLuminance: majorValueTo, minorLuminance: minorValueTo },
            duration,
            easing: allEasing,
        });
        symbols.push(firstSymbol);

        for (let i = 2; i < majorEffectValues.length; i++) {

            majorValueTo = majorEffectValues[i];
            minorValueTo = minorEffectValues[i];

            const symbol = new Symbol({
                //from: { majorLuminance: majorValue, minorLuminance : minorValue},
                to: {hue, saturation, majorLuminance: majorValueTo, minorLuminance: minorValueTo },
                duration,
                easing: allEasing,
            });

            symbols.push(symbol);
        }

        //auto set of needed props
        symbols.forEach((symbol) => {
            symbol.setContext(this.context);
            symbol.setTarget(this.target);
            symbol.setup(this.context);
        });
        //add symbols
        this.addSymbols(symbols);

        //console.log(symbols);
    }
}
