export class Transform {

    constructor(x, y, z, rotX, rotY, rotZ) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;

        this.rotateX = rotX || 0;
        this.rotateY = rotY || 0;
        this.rotateZ = rotZ || 0;
    }

    setElement(el) {
        this.el = el;
    }

    setTranslateX(x) {
        this.setTranslate(x, this.y, this.z);
    }
    setTranslateY(y) {
        this.setTranslate(this.x, y, this.z);
    }
    setTranslateZ(z) {
        this.setTranslate(this.x, this.y, z);
    }

    setTranslate(x, y, z) {
        this.x = x;
        this.y = y;
        if (z) {
            this.z = z;
        }
        this.apply();
    }

    setRotateX(x) {
        this.setRotate(x, this.rotateY, this.rotateZ);
    }
    setRotateY(y) {
        this.setRotate(this.rotateX, y, this.rotateZ);
    }
    setRotateZ(z) {
        this.setRotate(this.rotateX, this.rotateY, z);
    }
    setRotate(x, y, z) {
        this.rotateX = x;
        this.rotateY = y;
        this.rotateZ = z;
        this.apply();
    }

    apply() {
        this.el.style.transform = "translate3d(" + this.x + "px," + this.y + "px," + this.z + "px) " +
            "rotateX(" + this.rotateX + "deg) rotateY(" + this.rotateY + "deg) rotateZ(" + this.rotateZ + "deg)";
    }
}

//UTILS
function getRandomFromTo(from, to) {
    var result = Math.random() * (to - from) + from;
    return result; //on retourne la valeur de la variable result
}

function map(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}