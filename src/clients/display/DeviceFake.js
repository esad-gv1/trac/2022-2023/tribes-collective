import { Transform } from "./Transform.js";

export class DeviceFake {

    constructor({
        id = null,
        containerElement = null,
        imageFile = null,
        context = null,
        tribeName = null,
        name = null
    } = {}) {

        this.id = id;
        this.name = name;
        this.tribeName = tribeName;
        this.containerElement = containerElement;
        this.context = context;
        this.element = null;

        this.effects = [];

        this.currentZone = null;

        this.width = 50;
        this.height = 80;

        this.startX = 0;
        this.startY = 0;

        if (imageFile) {
            this.element = document.createElement("img");
            this.element.classList.add("device");
            this.element.classList.add("deviceImgIcon");
            this.element.src = imageFile;
        }
        else {
            this.element = document.createElement("div");
            this.element.classList.add("device");
            this.element.classList.add("deviceDivIcon");

            if (this.tribeName === "luminance") {
                this.element.classList.add("luminance");
            }
            else if (this.tribeName === "sound") {
                this.element.classList.add("sound");
            }
            else if (this.tribeName === "vibration") {
                this.element.classList.add("vibration");
            }
        }

        if (this.name) {
            this.element.innerHTML = `<bold>${this.name}</bold>`;
        }

        this.element.style.width = `${this.width}px`;
        this.element.style.height = `${this.height}px`;

        this.transform = new Transform();
        this.element.transform = this.transform;
        this.transform.setElement(this.element);

        this.containerElement.appendChild(this.element);

        this.element.addEventListener("pointerdown", this.onPointerDown.bind(this));
        this.element.addEventListener("pointerup", this.onPointerUp.bind(this));
    }

    addEffect(effect) {

        if (this.effects.indexOf(effect) < 0) {
            this.effects.push(effect);
            //this.innerHTML = "<bold>"+ this.name +"</bold>";
            this.element.innerHTML += `<br>${effect}`;
            console.log("add effect", this.effects);
        }
    }

    removeEffect(effect) {

        const effectIndex = this.effects.indexOf(effect);
        if (effectIndex >= 0) {
            this.effects.splice(effectIndex, 1);
        }
        this.element.innerHTML = `<bold>${this.name}</bold>`;
        console.log("remove effect", this.effects);
    }

    removeEffects() {
        this.effects = [];
        this.element.innerHTML = `<bold>${this.name}</bold>`;
    }

    onPointerDown(event) {
        //console.log(this,this.context, event)
        this.isSelected = true;
        this.context.selectedDevice = this;
        this.startX = event.offsetX;
        this.startY = event.offsetY;
        //console.log(this.startX, this.startY)
    }

    onPointerUp(event) {
        this.isSelected = false;
        this.context.selectedDevice = null;
        //console.log(this.isSelected, event)
    }

}