import * as Mobilizing from '@mobilizing/library';
import { Client } from '@mobilizing/platform/client/Client.js';
import { DisplayScript } from './DisplayScript.js';

export default class DisplayClient extends Client {
    constructor({
        features,
        container,
    } = {}) {
        super({
            features,
            container,
        });
        this.client = this;
        this.container = container;

        this.context = new Mobilizing.Context();
        this.mobilizingScript = new DisplayScript({
            reportCallback: (updates) => this.report(updates), // bind this
        });

        this.playerStates = new Map();
    }

    async init() {
        await super.init();
        // features may be available
    }

    async start() {
        await super.start();
        // available features should be ready

        // move soundworks container under
        this.container.style.zIndex = -100;
        this.container.style.position = 'absolute';

        // register to the already existing global state
        this.globalState = await this.client.stateManager.attach('global');

        //subscribe to state updates
        this.globalState.subscribe((updates) => {
            const { reload } = updates;
            if (reload) {
                window.location = window.location; // eslint-disable-line
            }
        });

        this.client.stateManager.observe(async (schemaName, stateId, nodeId) => {

            switch (schemaName) {

                case "player": {

                    //create a new player state
                    const state = await this.client.stateManager.attach(schemaName, stateId);
                    //console.log(state);

                    //player disappears
                    state.onDetach(() => {
                        this.mobilizingScript.removePlayer(stateId);
                        this.playerStates.delete(stateId);
                    });

                    //subscribe to updates coming from shared states
                    state.subscribe((updates) => {
                        //console.log(updates);
                        this.mobilizingScript.updatePlayer(stateId, updates);
                        /* if (this.playerStates.size > 0) {
                        } */
                    });

                    //adds new player to the main map
                    this.playerStates.set(stateId, state);
                    this.mobilizingScript.addPlayer(stateId, state);

                    console.log(this.playerStates);
                    break;
                }
                default:
                    break;
            }
        });

        //check the script for the need of user interaction
        if (this.mobilizingScript.needsUserInteraction) {
            this.context.userInteractionDone.setup();
            await this.context.userInteractionDone.promise;
        }

        this.context.addComponent(this.mobilizingScript);
        this.runner = new Mobilizing.Runner({
            context: this.context,
        });
    }

    //for data and events coming from Mobilizing user script
    report(updates) {
        const { id, device, reload } = updates;
        //console.log(id, device);

        //we speak to a particular device
        if (id && device) {
            const playerState = this.playerStates.get(id);
            //console.log(playerState);
            playerState.set({ device });
        }

        if (reload) {
            this.globalState.set({ reload: true });
        }
    }

}
