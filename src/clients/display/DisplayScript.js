import * as Mobilizing from '@mobilizing/library';
import { DeviceFake } from "./DeviceFake.js";

export class DisplayScript {

    constructor({
        reportCallback,
    } = {}) {

        this.reportCallback = reportCallback;

        this.needsUserInteraction = false;

        //this.container = document.getElementById("main-container");
        this.container = document.createElement("div");
        this.container.className = "main-container";
        document.body.appendChild(this.container);

        this.devices = new Map(); //all the devices
        this.devicesCurrentZone = new Map(); //intermediate Map to compute device.currentZone after intersections
        this.zoneDevices = new Map();//intermediate Map to compute what device is inside zones

        this.tribeMaxMembers = 3;
        this.tribeNames = ["luminance", "sound", "vibration"];

        this.selectedDevice = null;//-> set by FakeDevice inner pointerEvent

        //reload button
        const reloadBt = document.createElement("button");
        reloadBt.innerText = "reload";
        reloadBt.style.position = "fixed";
        reloadBt.style.top = "0px";
        reloadBt.style.left = "0px";
        reloadBt.addEventListener("click", () => {
            //this.globalState.set({ reload: true })
            this.report({ reload: true });
        });
        document.body.appendChild(reloadBt);

        //build the intersection zones
        this.zones = [];

        for (let i = 0; i < 9; i++) {
            const zone = document.createElement("div");
            zone.name = "zone" + i;
            zone.classList.add("zone");
            zone.classList.add("zone" + i);
            zone.innerText = zone.name;
            this.container.appendChild(zone);
            this.zones.push(zone);

            this.zoneDevices.set(zone.name, new Set());
        }

        console.log("this.zoneDevices", this.zoneDevices);
    }

    preLoad(loader) {
    }


    setup() {
        //inputs
        const touch = this.context.addComponent(new Mobilizing.input.Touch({ "target": this.container }));
        touch.setup();//set it up
        touch.on();//active it

        const mouse = this.context.addComponent(new Mobilizing.input.Mouse({ "target": this.container }));
        mouse.setup();//set it up
        mouse.on();//active it

        this.pointer = new Mobilizing.input.Pointer();
        this.context.addComponent(this.pointer);
        this.pointer.add(touch);
        this.pointer.add(mouse);
        this.pointer.setup();//set it up
        this.pointer.on();//active it

        this.baseX = null;
        this.baseY = null;

        this.pointer.events.on("moved", (p) => {
            this.onMoved(p);
        });

        this.pointer.events.on("off", (p) => {
            this.onOff(p);
        });
        //console.log(device);
    }

    onMoved(p) {
        //if we have a device selected, move it and check intersections with zones
        if (this.selectedDevice) {
            const x = p.x - this.selectedDevice.startX;
            const y = p.y - this.selectedDevice.startY;
            this.selectedDevice.transform.setTranslate(x, y);
            //check intersections with zones
            this.getIntersections();
        }
        //then refresh the data list for each zones
        this.generateZoneDevice();
    }

    onOff() {
        //on button release, compute data from intersections
        this.checkDevicesZones();
    }

    getIntersections() {

        //loop through all devices for dist computing
        this.devices.forEach((device) => {

            //reset currentZone
            device.currentZone = null;

            const deviceX = device.transform.x;
            const deviceY = device.transform.y;
            const deviceX2 = deviceX + device.element.offsetWidth;
            const deviceY2 = deviceY + device.element.offsetHeight;

            //console.log(deviceX, deviceY, deviceX2,deviceY2);

            const deviceP1 = new Mobilizing.three.Vector2(deviceX, deviceY);
            const deviceP2 = new Mobilizing.three.Vector2(deviceX2, deviceY);
            const deviceP3 = new Mobilizing.three.Vector2(deviceX2, deviceY2);
            const deviceP4 = new Mobilizing.three.Vector2(deviceX, deviceY2);
            const devicePoints = [deviceP1, deviceP2, deviceP3, deviceP4];

            this.zones.forEach((zone) => {
                //const zone = this.zones[0];
                const zoneX = zone.offsetLeft;
                const zoneY = zone.offsetTop;
                const zoneX2 = zoneX + zone.offsetWidth;
                const zoneY2 = zoneY + zone.offsetHeight;

                //console.log(zoneX, zoneY, zoneX2, zoneY2);

                const zoneP1 = new Mobilizing.three.Vector2(zoneX, zoneY);
                const zoneP2 = new Mobilizing.three.Vector2(zoneX2, zoneY);
                const zoneP3 = new Mobilizing.three.Vector2(zoneX2, zoneY2);
                const zoneP4 = new Mobilizing.three.Vector2(zoneX, zoneY2);
                const zonePoints = [zoneP1, zoneP2, zoneP3, zoneP4];

                //console.log(devicePoints, zonePoints);
                for (let i = 0; i < devicePoints.length; i++) {

                    const devicePoint = devicePoints[i];

                    if (devicePoint.x > zonePoints[0].x &&
                        devicePoint.x < zonePoints[1].x &&
                        devicePoint.y > zonePoints[0].y &&
                        devicePoint.y < zonePoints[3].y) {

                        device.currentZone = zone.name;

                        //console.log(device, device.currentZone);
                        break;
                    }
                }
            });

            this.devicesCurrentZone.set(device, device.currentZone);
            //console.log(this.devicesCurrentZone);
            //reset the effects for this device
            device.removeEffects();
        });
    }

    generateZoneDevice() {

        this.zoneDevices.forEach((devicesSet, zoneName) => {
            //console.log("zoneName",zoneName);
            this.zoneDevices.set(zoneName, new Set());
        });

        this.devicesCurrentZone.forEach((currentZone, device) => {

            //console.log("currentZone", currentZone, "device", device);
            if (currentZone) {
                const zoneSet = this.zoneDevices.get(currentZone);
                zoneSet.add(device);
            }
        });
        //console.log(this.zoneDevices);
    }

    checkDevicesZones() {

        this.zoneDevices.forEach((devicesSet, zoneName) => {

            // Un seul device = pas de changements
            if (devicesSet.size > 1) {

                //plus d'un device
                if (devicesSet.size >= 2) {
                    let device0, device1, device2;

                    const iterator = devicesSet.keys();
                    device0 = iterator.next().value;
                    device1 = iterator.next().value;

                    //on doit avoir des tribus différentes
                    if (device0.tribeName !== device1.tribeName) {
                        this.setDevicesState(device0, device1);
                    }

                    //3 devices sur la même zone
                    if (devicesSet.size === 3) {
                        device2 = iterator.next().value;

                        if (device0.tribeName !== device2.tribeName) {
                            this.setDevicesState(device0, device2);
                        }

                        if (device1.tribeName !== device2.tribeName) {
                            this.setDevicesState(device1, device2);
                        }
                    }

                    //console.log("device0", device0, "device1", device1, "device2", device2);
                }
            }
            else {
                const iterator = devicesSet.keys();
                const device0 = iterator.next().value;
                if (device0) {
                    console.log("remove effects for", device0);
                    device0.removeEffects();
                }
            }

        });

        //communicates back to the sharedClient
        //must be compiliant with the schema!
        this.devices.forEach((device) => {
            const schemaDevice = {
                tribeName: device.tribeName,
                name: device.name,
                effects: device.effects,
            };
            this.report({ id: device.id, device: schemaDevice });
        });

    }

    setDevicesState(device0, device1) {
        //luminance <-> sound
        if (device0.tribeName === "luminance" && device1.tribeName === "sound") {
            device0.addEffect("sound");
            device1.addEffect("luminance");
        }
        if (device0.tribeName === "sound" && device1.tribeName === "luminance") {
            device0.addEffect("luminance");
            device1.addEffect("sound");
        }

        //luminance <-> vibration
        if (device0.tribeName === "luminance" && device1.tribeName === "vibration") {
            device0.addEffect("vibration");
            device1.addEffect("luminance");
        }
        if (device0.tribeName === "vibration" && device1.tribeName === "luminance") {
            device0.addEffect("luminance");
            device1.addEffect("vibration");
        }

        //sound <-> vibration
        if (device0.tribeName === "sound" && device1.tribeName === "vibration") {
            device0.addEffect("vibration");
            device1.addEffect("sound");
        }
        if (device0.tribeName === "vibration" && device1.tribeName === "sound") {
            device0.addEffect("sound");
            device1.addEffect("vibration");
        }
    }

    update() { }

    /*=============
    *  Players (mobile)
    * =============
    */
    addPlayer(stateId, state) {
        //console.log("stateId", stateId, "state", state);

        //count the total nb of devices already here
        const deviceTotal = this.devices.size;

        //check for each tribe
        let luminanceCount = 0;
        let soundCount = 0;
        let vibrationCount = 0;

        //if we have some body here already, count the nb of device for each tribe
        if (deviceTotal > 0) {
            this.devices.forEach((device) => {
                switch (device.tribeName) {
                    case "luminance":
                        luminanceCount++;
                        break;
                    case "sound":
                        soundCount++;
                        break;
                    case "vibration":
                        vibrationCount++;
                        break;
                    default:
                        break;
                }
            });
        }

        //extract the index of the current tribe to give to the new user
        let tribeIndex = null;
        if (luminanceCount < 3) {
            tribeIndex = 0;
        }
        else if (soundCount < 3) {
            tribeIndex = 1;
        }
        else if (vibrationCount < 3) {
            tribeIndex = 2;
        }
        //console.log("tribe index not full is", tribeIndex);

        //get the tribe name to give
        const tribeName = this.tribeNames[tribeIndex];
        //console.log("tribeName is", tribeName);

        const foundIndexes = [];
        for (let i = 0; i < this.tribeMaxMembers; i++) {
            foundIndexes[i] = null;
        }

        //check for the current tribe what nb is empty
        for (let i = 0; i < this.tribeMaxMembers; i++) {
            // eslint-disable-next-line no-loop-func
            this.devices.forEach((device) => {
                if (device.tribeName === tribeName) { //do we have the right tribe name ?
                    const deviceTribeIndex = device.name.substring(device.name.length - 1);//then get the index from name string
                    foundIndexes[deviceTribeIndex] = Number(deviceTribeIndex);//switch from null to index
                    //console.log("deviceTribeIndex", deviceTribeIndex, "foundIndexes", foundIndexes);
                }
            });
        }

        let indexNotFound = 0;
        for (let i = 0; i < this.tribeMaxMembers; i++) {
            if (foundIndexes[i] === null) {
                indexNotFound = i;
                break;
            }
        }
        //console.log("tribeName :", tribeName, "indexNotFound :", indexNotFound);

        const device = new DeviceFake({
            id: stateId,
            containerElement: this.container,
            context: this, //current script
            tribeName,
            name: `${tribeName}${indexNotFound}`,
        });

        const x = Mobilizing.math.randomFromTo(300, this.container.offsetWidth - device.element.offsetWidth - 300);
        const y = Mobilizing.math.randomFromTo(100 + device.element.offsetHeight, this.container.offsetHeight - device.element.offsetHeight - 100);
        /* const x = this.container.offsetWidth / 2 - device.element.offsetWidth;
        const y = this.container.offsetHeight / 2 - device.element.offsetHeight; */

        device.transform.setTranslate(x, y);

        this.devices.set(stateId, device);
        console.log("new device :", device, x, y);
        console.log("devices", this.devices, "\n\n");

        //and recheck states intersections
        this.getIntersections();
        this.generateZoneDevice();
        this.checkDevicesZones();

        //communicates back to the sharedClient
        //must be compiliant with the schema!
        const schemaDevice = {
            tribeName: device.tribeName,
            name: device.name,
            effects: []
        };
        this.report({ id: stateId, device: schemaDevice });
    }

    removePlayer(stateId) {

        const device = this.devices.get(stateId);
        console.log("removes", device);
        //remove is from list
        device.element.remove();
        this.devices.delete(stateId);

        //resets everything to clean states
        this.resetAllDataMap();

        //and recheck all states intersections
        this.getIntersections();
        this.generateZoneDevice();
        this.checkDevicesZones();
    }

    //clear all Maps used to computed states intersections
    resetAllDataMap() {
        this.devices.forEach((dev) => {
            //reset currentZone
            dev.currentZone = null;
            dev.removeEffects();
        });
        this.zoneDevices.forEach((deviceSet, zoneName) => {
            this.zoneDevices.set(zoneName, new Set());
        });
        this.devicesCurrentZone.forEach((currentZone, dev) => {
            this.devicesCurrentZone.set(dev, null);
        });
    }

    //
    updatePlayer(stateId, updates) {
    }

    report(updates) {
        if (!this.reportCallback) {
            return;
        }
        // direct binding
        this.reportCallback(updates);
    }

}
