Tribes Collective

Projet T|RA|C

Esad•Valence
Esisar
Conservatoire de Valence

# Usage

## Cloner ce répertoire

> git clone https://gitlab.com/esad-gv1/trac/2022-2023/tribes-collective.git

## Compiler les dépendances : DNSmasq

Lire le fichier `platform-helpers/ReadMe.md` et suivre les instructions pour configurer le réseau local.

Lancer les deux scripts suivants pour lancer DNSMasq :

> ./platform-helpers/dns/DNS_run_local.command
> ./platform-helpers/dns/DNS_run_public.command

## Installer les dépendances du projets

> npm install

## Lancer le projet

> npm run dev

Attendre que le serveur soit lancer (l'information apparaît dans le terminal) et ouvrir un navigateur avec les adresses suivantes :


Pour le display **NB : doit être ouvert en premier!** = `https://dev.mobilizing-js.net:8000/display` 

Pour un "player" (smartphone) = `https://dev.mobilizing-js.net:8000/`

